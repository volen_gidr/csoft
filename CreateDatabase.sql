IF DB_ID('PhoneBookDB') IS NOT NULL
	DROP DATABASE PhoneBookDB;
GO

CREATE DATABASE PhoneBookDB
		ON 
		(
			Name = PhoneBookDB,
			FILENAME = 'C:\Users\CSoft\Desktop\Volen\Repo\PhoneBookDB.mdf',
			SIZE = 30,
			FILEGROWTH = 10 
		 );
GO