#include "stdafx.h"

#define NAME_SIZE 32
#define NUMBER_SIZE 16
#define ADDRESS_SIZE 255
#define UCN_SIZE 10

/// <summary>
/// Структура описваща таблицата PERSONS
/// </summary>
struct PERSONS
{
	int  nID;
	int  nUpdateCounter;
	char szFirstName[NAME_SIZE];
	char szMiddleName[NAME_SIZE];
	char szLastName[NAME_SIZE];
	char szUCN[UCN_SIZE];
	int  nCityID;
	char szAddress[ADDRESS_SIZE];

	PERSONS()
	{
		SecureZeroMemory(this, sizeof(PERSONS));
	}

	//Constructor used in example
	PERSONS(int nID, int nUpdateCounter, char szFirstName[NAME_SIZE], char szMiddleName[NAME_SIZE], char szLastName[NAME_SIZE], char szUCN[UCN_SIZE], int nCityID, char szAddress[ADDRESS_SIZE])
	{
		SecureZeroMemory(this, sizeof(PERSONS));
			
		this->nID = nID;
		this->nUpdateCounter = nUpdateCounter;
		this->nCityID = nCityID;

		for ( int i = 0; i < NAME_SIZE ; i++ )
		{
			this->szFirstName[i] = szFirstName[i];
			this->szMiddleName[i] = szMiddleName[i];
			this->szLastName[i] = szLastName[i];
		}
		
		for ( int i = 0; i < UCN_SIZE ; i++ )
		{
			this->szUCN[i] = szUCN[i];
		}

		for ( int i = 0; i < ADDRESS_SIZE; i++ )
		{
			this->szAddress[i] = szAddress[i];
		}
	}
};

typedef CTypedPtrArray<CPtrArray, PERSONS*> CPersonsArray;

/// <summary>
/// Структура описваща таблицата CITIES
/// </summary>
struct CITIES
{
	int  nID;
	int  nUpdateCounter;
	char szName[NAME_SIZE];
	char szArea[NAME_SIZE];

	CITIES()
	{
		SecureZeroMemory(this, sizeof(CITIES));
	}
};

typedef CTypedPtrArray<CPtrArray, CITIES> CCitiesArray;


/// <summary>
/// Структура описваща таблицата PHONE_TYPES
/// </summary>
struct PHONE_TYPES
{
	int  nID;
	int  nUpdateCounter;
	char szPhoneType[NAME_SIZE];

	PHONE_TYPES()
	{
		SecureZeroMemory(this, sizeof(PHONE_TYPES));
	}
};

typedef CTypedPtrArray<CPtrArray, PHONE_TYPES> CPhoneTypesArray;


/// <summary>
/// Структура описваща таблицата PHONE_NUMBERS
/// </summary>
struct PHONE_NUMBERS
{
	int  nID;
	int  nUpdateCounter;
	int  nPersonID;
	int  nPhoneTypeID;
	char szPhoneNumber[NUMBER_SIZE];

	PHONE_NUMBERS()
	{
		SecureZeroMemory(this, sizeof(PHONE_NUMBERS));
	}
};

typedef CTypedPtrArray<CPtrArray, PHONE_NUMBERS> CPhoneNumbersArray;