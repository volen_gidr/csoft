// PhoneTypesTable.h: interface of the CPhoneTypesTable class
//
#include "Structures.h"
#include <atldbcli.h>
#pragma once

class CPhoneTypesAccessor
{
protected:
	PHONE_TYPES m_PhoneType;

	BEGIN_ACCESSOR_MAP(CPhoneTypesAccessor, 2)
		BEGIN_ACCESSOR(0, true)
			COLUMN_ENTRY(1, m_PhoneType.lID)
		END_ACCESSOR()

		BEGIN_ACCESSOR(1, true)
			COLUMN_ENTRY(2, m_PhoneType.lUpdateCounter)
			COLUMN_ENTRY(3, m_PhoneType.szPhoneType)
		END_ACCESSOR()
	END_ACCESSOR_MAP()
};

class CPhoneTypesTable : public CCommand<CAccessor<CPhoneTypesAccessor> >
{
public:
	BOOL SelectAll(CPhoneTypesArray& oPhoneTypesArray);
	BOOL DeleteWhereID(const long lID);
	BOOL SelectWhereID(const long lID, PHONE_TYPES& oPhoneType);
	BOOL UpdateWhereID(const long lID, const PHONE_TYPES& oPhoneType);
	BOOL Insert(const PHONE_TYPES& oPhoneType);
private:
	BOOL ConnectToDatabase(CDataSource& oDataSource, CSession& oSession);
	BOOL CreateModifyingSession(CString strQuery, CSession& oSession);
};