// PhoneNumbersTable.h: interface of the CPhoneNumbersTable class
//
#include "Structures.h"
#include <atldbcli.h>
#pragma once

class CPhoneNumbersAccessor
{
	protected:
	PHONE_NUMBERS m_PhoneNumber;

	BEGIN_ACCESSOR_MAP(CPhoneNumbersAccessor, 2)
		BEGIN_ACCESSOR(0, true)
			COLUMN_ENTRY(1, m_PhoneNumber.lID)
		END_ACCESSOR()

		BEGIN_ACCESSOR(1, true)
			COLUMN_ENTRY(2, m_PhoneNumber.lUpdateCounter)
			COLUMN_ENTRY(3, m_PhoneNumber.lPersonID)
			COLUMN_ENTRY(3, m_PhoneNumber.lPhoneTypeID)
			COLUMN_ENTRY(4, m_PhoneNumber.szPhoneNumber)
		END_ACCESSOR()
	END_ACCESSOR_MAP()
};

class CPhoneNumbersTable : public CCommand<CAccessor<CPhoneNumbersAccessor>>
{
	public:
	BOOL SelectAll(CPhoneNumbersArray& oPhoneNumbersArray);
	BOOL DeleteWhereID(const long lID);
	BOOL SelectWhereID(const long lID, PHONE_NUMBERS& oPhoneNumber);
	BOOL UpdateWhereID(const long lID, const PHONE_NUMBERS& oPhoneNumber);
	BOOL Insert(const PHONE_NUMBERS& oPhoneNumber);
private:
	BOOL ConnectToDatabase(CDataSource& oDataSource, CSession& oSession);
	BOOL CreateModifyingSession(CString strQuery, CSession& oSession);
};