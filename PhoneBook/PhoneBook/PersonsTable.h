// PersonsTable.h: interface of the CPersonsTable class
//
#include "Structures.h"
#include <atldbcli.h>
#pragma once

class CPersonsTableAccessor
{
protected: 
	PERSONS m_Person;

	BEGIN_ACCESSOR_MAP(CPersonsTableAccessor, 2)
		BEGIN_ACCESSOR(0,true)
			COLUMN_ENTRY(1, m_Person.lID)
		END_ACCESSOR()

		BEGIN_ACCESSOR(1,true)
			COLUMN_ENTRY(2, m_Person.lUpdateCounter)
			COLUMN_ENTRY(3, m_Person.szFirstName)
			COLUMN_ENTRY(4, m_Person.szMiddleName)
			COLUMN_ENTRY(5, m_Person.szLastName)
			COLUMN_ENTRY(6, m_Person.szUCN)
			COLUMN_ENTRY(7, m_Person.lCityID)
			COLUMN_ENTRY(8, m_Person.szAddress)
		END_ACCESSOR()
	END_ACCESSOR_MAP()
};

class CPersonsTable : public CCommand<CAccessor<CPersonsTableAccessor>>
{
public:
	BOOL SelectAll(CPersonsArray& oPersonsArray);
	BOOL DeleteWhereID(const long lID);
	BOOL SelectWhereID(const long lID, PERSONS& oPerson);
	BOOL UpdateWHereID(const long lID, const PERSONS& oPerson);
	BOOL Insert(const PERSONS& oPerson);
private:
	BOOL ConnectToDatabase(CDataSource& oDataSource, CSession& oSession);
	BOOL CreateModifyingSession(CString strQuery, CSession& oSession);
};