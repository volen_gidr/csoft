
// CitiesTable.h: interface of the CCitiesTable class
//
#include "Structures.h"
#include <atldbcli.h>
#pragma once

class CCitiesTableAccessor
{
	protected:
	// data elements
	CITIES m_oCity;
	
	BEGIN_ACCESSOR_MAP(CCitiesTableAccessor, 2)
		BEGIN_ACCESSOR(0, true)
			COLUMN_ENTRY(1, m_oCity.lID)
		END_ACCESSOR()

		BEGIN_ACCESSOR(1, true)
			COLUMN_ENTRY(2, m_oCity.lUpdateCounter)
			COLUMN_ENTRY(3, m_oCity.szName)
			COLUMN_ENTRY(4, m_oCity.szArea)
		END_ACCESSOR()
	END_ACCESSOR_MAP()
};

class CCitiesTable : public CCommand<CAccessor<CCitiesTableAccessor> >
{
public:
	CCitiesTable();
	~CCitiesTable();

public:
	/// <summary>
	/// Public �����
	/// ����� ������ ������ �� ��������� CITIES
	/// </summary>
	BOOL SelectAll(CCitiesArray& oCitiesArray);

	/// <summary>
	/// Public �����
	/// ������� ����� � ���������� ID
	/// </summary>
	BOOL DeleteWhereID(const long lID);

	/// <summary>
	/// Public �����
	/// ����� ����� � ���������� ID 
	/// </summary>
	BOOL SelectWhereID(const long lID, CITIES& oCities);

	/// <summary>
	/// Public �����
	/// ������� �� ����� � ���������� ID 
	/// </summary>
	BOOL UpdateWhereID(const long lID, const CITIES& oCities);

	/// <summary>
	/// Public �����
	/// ������ �����
	/// </summary>
	BOOL Insert(const CITIES& oCities);
private:
	CDataSource m_oDataSource;
	CSession m_oSession;

	/// <summary>
	/// Private �����
	/// ����������� ������ � ���� �������
	/// </summary>
	BOOL ConnectToDatabase();
	
	/// <summary>
	/// Private �����
	/// ������� rowset
	/// </summary>
	BOOL CCitiesTable::FetchRowsetWhereID(const long lID, bool bModifying);

	///<summary>
	/// Private �����
	/// ��������� �������� � ������ ����� � ����������� �� �������
	///</summary>
	BOOL CCitiesTable::CheckConnection();

	///<summary>
	/// Private �����
	/// ��������� � ������� ��������� �� ������
	///</summary>
	void CCitiesTable::Message(CString strMessage, CString strQuery, HRESULT hResult);
};

