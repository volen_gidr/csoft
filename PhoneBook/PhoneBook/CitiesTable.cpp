
// CitiesTable.cpp : implementation of the CCitiesTable class
//
#include "stdafx.h"
#include "PhoneBook.h"
#include "CitiesTable.h"
#include "afxdb.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCitiesTable implementation


/// <summary>
/// �����������
/// </summary>
CCitiesTable::CCitiesTable()
{
	if(!ConnectToDatabase())
	{
		m_oDataSource.Close();
		m_oSession.Close();
		Close();
	}
}

/// <summary>
/// ����������
/// </summary>
CCitiesTable::~CCitiesTable()
{
	m_oDataSource.Close();
	m_oSession.Close();
	Close();
}

/// <summary>
/// Public �����
/// ������� �� ����� � ���������� ID 
/// </summary>
BOOL CCitiesTable::UpdateWhereID(const long lID, const CITIES& oCity)
{
	if(!CheckConnection())
	{
		return FALSE;
	}

	if(!FetchRowsetWhereID(lID, TRUE))
	{
		return FALSE;
	}

	// ��� �� ������ ��������
	if(m_oCity.lUpdateCounter != oCity.lUpdateCounter)
	{
		Message(_T("������. ������� �� ��������� �� 3�� ����."), NULL, NULL);
		return FALSE;
	}

	m_oCity = oCity;
	m_oCity.lUpdateCounter+=1;

	HRESULT hResult = SetData(1);

	if(FAILED(hResult))
	{
		//Error setting data
		Message(_T("������ ��� ������� �� �����. ���������: %s."), NULL, hResult);
		return FALSE;
	}

	return TRUE;
};

/// <summary>
/// Public �����
/// ������ �����
/// </summary>
BOOL CCitiesTable::Insert(const CITIES& oCity)
{
	if(!CheckConnection())
	{
		return FALSE;
	}

	if(!FetchRowsetWhereID(0, TRUE))
	{
		return FALSE;
	}

	//��������� �������� � m_oCity
	m_oCity = CITIES(oCity);

	HRESULT hResult = CRowset::Insert(1,true);

	if (FAILED(hResult))
	{
		//Error inserting data
		Message(_T("������ ��� ��������� �� �����. ���������: %s"), NULL, hResult);
		return FALSE;
	} 

	return TRUE;
}

/// <summary>
/// Public �����
/// ����� ����� � ���������� ID 
/// </summary>
BOOL CCitiesTable::SelectWhereID(const long lID, CITIES& oCity)
{
	if(!CheckConnection())
	{
		return FALSE;
	}

	if(!FetchRowsetWhereID(lID, FALSE))
	{
		return FALSE;
	}

	//��������� �������� � oCity
	oCity = m_oCity;

	return TRUE;
} 

/// <summary>
/// Public �����
/// ������� ����� � ���������� ID
/// </summary>
BOOL CCitiesTable::DeleteWhereID(const long lID)
{
	if(!CheckConnection())
	{
		return FALSE;
	}

	if(!FetchRowsetWhereID(lID, TRUE))
	{
		return FALSE;
	}

	HRESULT hResult = Delete();

	if(FAILED(hResult))
	{
		Message(_T("������ ��� ��������� �� ������. ���������: %s"), NULL, hResult);
		return FALSE;
	}

	return TRUE;
}

/// <summary>
/// Public �����
/// ����� ������ ������ �� ��������� CITIES
/// </summary>
BOOL CCitiesTable::SelectAll(CCitiesArray& oCityArray)
{
	if(!CheckConnection())
	{
		return FALSE;
	}

	if(!FetchRowsetWhereID(0, FALSE))
	{
		return FALSE;
	}

	HRESULT hResult = S_FALSE;	

	while( (hResult = MoveNext()) == S_OK)
	{
		CITIES* pCity = new CITIES(m_oCity);
		oCityArray.Add(pCity);
	}

	//�������� �� ����
	if(hResult != DB_S_ENDOFROWSET )
	{
		Message(_T("������ ��� �������� �� �������. ���������: %s"), NULL, hResult);
		return FALSE;
	}

	return TRUE;
}

/// <summary>
/// Private �����
/// ����������� ������ � ���� �������
/// </summary>
BOOL CCitiesTable::ConnectToDatabase()
{
	CDBPropSet oDBPropSet(DBPROPSET_DBINIT);
	oDBPropSet.AddProperty(DBPROP_INIT_DATASOURCE,	_T("LENOVO2\\SQL2008") );	// ������
	oDBPropSet.AddProperty(DBPROP_AUTH_USERID,		_T("sa") );			// ����������
	oDBPropSet.AddProperty(DBPROP_AUTH_PASSWORD,	_T("massive") );			// ������
	oDBPropSet.AddProperty(DBPROP_INIT_CATALOG,		_T("PhoneBookDB" ));	// ���� �����
	oDBPropSet.AddProperty(DBPROP_AUTH_PERSIST_SENSITIVE_AUTHINFO,	false);
	oDBPropSet.AddProperty(DBPROP_INIT_LCID,		1033L);
	oDBPropSet.AddProperty(DBPROP_INIT_PROMPT,		static_cast<short>(4));

	// ��������� �� ��� ������ �����
	HRESULT hResult = m_oDataSource.Open(_T("SQLOLEDB.1"), &oDBPropSet);

	if (FAILED(hResult))
	{
		return FALSE;
	}

	// �������� �����
	hResult = m_oSession.Open(m_oDataSource);

	if (FAILED(hResult))
	{
		return FALSE;
	}

	return TRUE;
}


/// <summary>
/// Private �����
/// ������� rowset
/// </summary>
BOOL CCitiesTable::FetchRowsetWhereID(const long lID, bool bModifying)
{
	//������������ ��������
	CString strQuery;
	if(lID == 0)
	{
		strQuery = _T("SELECT * FROM CITIES");
	}
	else
	{
		strQuery.Format(_T("SELECT * FROM CITIES WHERE ID = %d"), lID);
	}
	
	HRESULT hResult = E_FAIL;


	CCommand::Close();

	if(!bModifying)
	{
		// ����������� ���������
		hResult = Open(m_oSession, strQuery);
	}
	else
	{
		CDBPropSet oUpdateDBPropSet(DBPROPSET_ROWSET);
		oUpdateDBPropSet.AddProperty(DBPROP_CANFETCHBACKWARDS, true);
		oUpdateDBPropSet.AddProperty(DBPROP_IRowsetScroll, true);
		oUpdateDBPropSet.AddProperty(DBPROP_IRowsetChange, true);
		oUpdateDBPropSet.AddProperty(DBPROP_UPDATABILITY, DBPROPVAL_UP_CHANGE | DBPROPVAL_UP_INSERT | DBPROPVAL_UP_DELETE);


		// ����������� ���������
		hResult = Open(m_oSession, strQuery, &oUpdateDBPropSet);
	}	

	if (FAILED(hResult))
	{
		Message(_T("������ ��� ���������� �� ���������: %s. ���������: %s"), strQuery, hResult); 
		return FALSE;
	}

	if(lID != 0)
	{
		hResult = MoveNext();
	}

	if (FAILED(hResult) || hResult == DB_S_ENDOFROWSET)
	{
		if(hResult == DB_S_ENDOFROWSET)
			Message(_T("������� �� � �������!"), NULL, NULL);
		else
			Message(_T("������ ��� ��������� �� ������: %s"), NULL, hResult);
		return FALSE;
	}

	return TRUE;
}

///<summary>
/// Private �����
/// ��������� �������� � ������ ����� � ����������� �� �������
///</summary>
BOOL CCitiesTable::CheckConnection()
{
	if (m_oDataSource.m_spInit == NULL)
	{
		Message(_T("������ ��� ��������� � ���� �������"), NULL, NULL);
		return FALSE;
	}

	if ( m_oSession.m_spOpenRowset == NULL )
	{
		Message(_T("������ ��� �������� �� �����"), NULL, NULL);
		return FALSE;
	}

	return TRUE;
}

///<summary>
/// Private �����
/// ��������� � ������� ��������� �� ������
///</summary>
void CCitiesTable::Message(CString strMessage, CString strQuery, HRESULT hResult)
{
	_com_error err(hResult);
	CString strErrorMessage = err.ErrorMessage();

	CString strError;
	if(strQuery.IsEmpty() && hResult == NULL)
		strError = strMessage;

	else if(strQuery.IsEmpty())
		strError.Format(strMessage, strErrorMessage);

	else if(hResult == NULL)
		strError.Format(strMessage, strQuery);

	else 
		strError.Format(strMessage, strQuery, strErrorMessage);

	MessageBox(NULL, strError, _T("������"), MB_ICONERROR);
}
