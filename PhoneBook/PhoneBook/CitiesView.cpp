
// CitiesView.cpp : implementation of the CCitiesView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "PhoneBook.h"
#endif

#include "CitiesDoc.h"
#include "CitiesView.h"
#include "CitiesTable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCitiesView

IMPLEMENT_DYNCREATE(CCitiesView, CListView)

BEGIN_MESSAGE_MAP(CCitiesView, CListView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CCitiesView construction/destruction

CCitiesView::CCitiesView()
{
	CCitiesTable oCitiesTable;
	//����� ������ ������ �� CITIES � oCCitiesArray
	CCitiesArray oCitiesArray;
	oCitiesTable.SelectAll(oCitiesArray);
	CITIES* temp = oCitiesArray.GetAt(0);
	//����� ������ � ID=3 � oCity
	CITIES oCity;
	oCitiesTable.SelectWhereID(1, oCity);

	// ���������� ���������
	_tcscpy_s(oCity.szName, _T("Plovdiv"));
	
	//�������  ������ � ID=3 � ����������� �� ������ ����� �� oCity1
	oCitiesTable.UpdateWhereID(oCity.lID, oCity);

	//������� ������ � ID=2
	oCitiesTable.DeleteWhereID(2);

	//������ ������ ����� �� oCity1 � CITIES
	CITIES oCity1(0,0,_T("TestNew2"),_T("TestNew2"));
	for( int i = 0 ; i < 10 ; i++ )
	{
		oCitiesTable.Insert(oCity1);
	}

	oCitiesTable.SelectWhereID(7, oCity);

	oCitiesTable.DeleteWhereID(4);

	oCitiesTable.UpdateWhereID(7, oCity);

	for( int i = 0 ; i < 10 ; i++ )
	{
		oCitiesTable.Insert(oCity1);
	}

	oCitiesTable.SelectAll(oCitiesArray);

	oCitiesTable.DeleteWhereID(8);
}

CCitiesView::~CCitiesView()
{
}

BOOL CCitiesView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CListView::PreCreateWindow(cs);
}

void CCitiesView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();


	// TODO: You may populate your ListView with items by directly accessing
	//  its list control through a call to GetListCtrl().
}

void CCitiesView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CCitiesView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CCitiesView diagnostics

#ifdef _DEBUG
void CCitiesView::AssertValid() const
{
	CListView::AssertValid();
}

void CCitiesView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CCitiesDoc* CCitiesView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCitiesDoc)));
	return (CCitiesDoc*)m_pDocument;
}
#endif //_DEBUG


// CCitiesView message handlers