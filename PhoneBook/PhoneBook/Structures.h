﻿#include "stdafx.h"

#define NAME_SIZE 32
#define MIDDLE_NAME_SIZE 32
#define LAST_NAME_SIZE 32
#define PHONE_TYPE_SIZE 32
#define AREA_SIZE 32
#define NUMBER_SIZE 16
#define ADDRESS_SIZE 255
#define UCN_SIZE 10

/// <summary>
/// Структура описваща таблицата PERSONS
/// </summary>
struct PERSONS
{
//Уникален идентификатор
	long  lID;
	//Брояч на ъпдейтите
	long  lUpdateCounter;
	//Малко име
	char szFirstName[NAME_SIZE+1];
	//Бащино име
	char szMiddleName[MIDDLE_NAME_SIZE+1];
	//Фамилия
	char szLastName[LAST_NAME_SIZE+1];
	//ЕГН
	char szUCN[UCN_SIZE+1];
	//Идентификатор на града
	long  lCityID;
	//Адрес
	char szAddress[ADDRESS_SIZE+1];

	//Нулиране на паметта
	PERSONS()
	{
		SecureZeroMemory(this, sizeof(PERSONS));
	}
	
	/// <summary>
	///Конструктор изпозлван в примерите
	/// </summary>
	PERSONS(long lID, long lUpdateCounter, char szFirstName[NAME_SIZE], char szMiddleName[NAME_SIZE], char szLastName[NAME_SIZE], char szUCN[UCN_SIZE], int nCityID, char szAddress[ADDRESS_SIZE])
	{
		SecureZeroMemory(this, sizeof(PERSONS));
		this->lID = lID;
		this->lUpdateCounter = lUpdateCounter;
		this->lCityID = nCityID;
		memcpy(this->szFirstName, szFirstName, sizeof(this->szFirstName));
		memcpy(this->szMiddleName, szMiddleName, sizeof(this->szMiddleName));
		memcpy(this->szLastName, szLastName, sizeof(this->szLastName));
		memcpy(this->szUCN, szUCN, sizeof(this->szUCN));
	}
};

//Дефиниция тип. Масив от указатили PERSONS*
typedef CTypedPtrArray<CPtrArray, PERSONS*> CPersonsArray;

/// <summary>
/// Структура описваща таблицата CITIES
/// </summary>
struct CITIES
{
	//Уникален идентификатор
	long  lID;
	//Брояч на ъпдейтите
	long  lUpdateCounter;
	//Име на града
	TCHAR szName[NAME_SIZE+1];
	//Район на града
	TCHAR szArea[NAME_SIZE+1];

	//Нулиране на паметта
	CITIES()
	{
		SecureZeroMemory(this, sizeof(CITIES));
	}

	CITIES(long lID, long lUpdateCounter, TCHAR szName[NAME_SIZE+1], TCHAR szArea[NAME_SIZE+1])
	{
		SecureZeroMemory(this, sizeof(CITIES));
		this->lID = lID;
		this->lUpdateCounter = lUpdateCounter;
		memcpy(this->szName, szName, sizeof(this->szName));
		memcpy(this->szArea, szArea, sizeof(this->szArea));
	}
};

//Дефиниция тип. Масив от указатили CITIES*
typedef CTypedPtrArray<CPtrArray, CITIES*> CCitiesArray;


/// <summary>
/// Структура описваща таблицата PHONE_TYPES
/// </summary>
struct PHONE_TYPES
{
	//Уникален идентификатор
	long  lID;
	//Брояч на ъпдейтите
	long  lUpdateCounter;
	//Вид на телефонния номер
	char szPhoneType[NAME_SIZE+1];

	//Нулиране на паметта
	PHONE_TYPES()
	{
		SecureZeroMemory(this, sizeof(PHONE_TYPES));
	}

	PHONE_TYPES(long lID, long lUpdateCounter, char szPhoneType[PHONE_TYPE_SIZE+1])
	{
		SecureZeroMemory(this, sizeof(PHONE_TYPES));
		this->lID = lID;
		this->lUpdateCounter = lUpdateCounter;
		memcpy(this->szPhoneType, szPhoneType, sizeof(szPhoneType));
	}
};

//Дефиниция тип. Масив от указатили PHONE_TYPES*
typedef CTypedPtrArray<CPtrArray, PHONE_TYPES*> CPhoneTypesArray;


/// <summary>
/// Структура описваща таблицата PHONE_NUMBERS
/// </summary>
struct PHONE_NUMBERS
{
	//Уникален идентификатор
	long  lID;
	//Брояч на ъпдейтите
	long  lUpdateCounter;
	//Идентификатор на потребителя
	long  lPersolID;
	//Идентификатор на видя на телефонния номер
	long  lPhoneTypeID;
	//Телефонен номер
	char szPhoneNumber[PHONE_TYPE_SIZE+1];

	//Нулиране на паметта
	PHONE_NUMBERS()
	{
		SecureZeroMemory(this, sizeof(PHONE_NUMBERS));
	}

	PHONE_NUMBERS(long lID, long lUpdateCounter, long lPersolID, long lPhoneTypeID, char szPhoneNumber[PHONE_TYPE_SIZE+1])
	{
		SecureZeroMemory(this, sizeof(PHONE_NUMBERS));
		this->lID = lID;
		this->lUpdateCounter = lUpdateCounter;
		this->lPersolID = lPersolID;
		this->lPhoneTypeID = lPhoneTypeID;
		memcpy(this->szPhoneNumber, szPhoneNumber, sizeof(szPhoneNumber));
	}
};

//Дефиниция тип. Масив от указатили PHONE_NUMBERS*
typedef CTypedPtrArray<CPtrArray, PHONE_NUMBERS*> CPhoneNumbersArray;