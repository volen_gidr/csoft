// PhoneTypesTable.cpp : implementation of the CPhoneTypesTable class
//
#include "stdafx.h"
#include "PhoneBook.h"
#include "PhoneTypesTable.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/// <summary>
/// Public �����
/// ������� ����� � ���������� ID
/// </summary>
BOOL CPhoneTypesTable::DeleteWhereID(const long lID)
	{
		CDataSource oDataSource;
		CSession oSession;

		try
		{
			HRESULT hResult = CoInitialize(0);

			if (FAILED(hResult))
			{
				//Message( _T("Unable to CoInitialize COM Interface.") );
				return FALSE;
			}

			if(!ConnectToDatabase(oDataSource, oSession))
			{
				//Error connecting to the database
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			//������������ ��������
			CString strQuery;
			strQuery.Format(_T("SELECT * FROM PHONE_TYPES WHERE ID=%d"),lID);

			if(!CreateModifyingSession(strQuery,oSession))
			{
				//Error opening session
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			hResult = MoveNext();

			if(FAILED(hResult))
			{
				//Error empty table
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			hResult = Delete();
			if(FAILED(hResult))
			{
				//Error deleting
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			hResult = Update();
			if(FAILED(hResult))
			{
				//Error updating table
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			// ��������� ���������, ������� � �������� � ������ �����. 
			oSession.Close();
			oDataSource.Close();
			Close();
		}
		catch(HRESULT)
		{
			return FALSE;
		}
		return TRUE;
	}

/// <summary>
/// Public �����
/// ����� ������ ������ �� ��������� CITIES
/// </summary>
BOOL CPhoneTypesTable::SelectAll(CPhoneTypesArray& oPhoneTypesArray)
	{
		CDataSource oDataSource;
		CSession oSession;

		try	
		{
			HRESULT hResult = CoInitialize(0);

			if (FAILED(hResult))
			{
				//Message( _T("Unable to CoInitialize COM Interface.") );
				return FALSE;
			}

			if(!ConnectToDatabase(oDataSource, oSession))
			{
				//Error connecting to the database
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			CString strQuery;
			strQuery = _T("SELECT * FROM PHONE_TYPES");

			hResult = Open(oSession, strQuery);


			if (FAILED(hResult))
			{
				//Message(_T("Error executing query. Error: %d. Query: %s"), hResult, strQuery);
				oSession.Close();
				oDataSource.Close();
				return FALSE;
			}
			

			while(MoveNext() == S_OK)
			{
				PHONE_TYPES* oPhoneType = new PHONE_TYPES(m_PhoneType.lID, m_PhoneType.lUpdateCounter, m_PhoneType.szPhoneType);
				oPhoneTypesArray.Add(oPhoneType);
			}

			// ��������� ���������, ������� � �������� � ������ �����. 
			Close();
			oSession.Close();
			oDataSource.Close();
		}
		catch (HRESULT)
		{
			return FALSE;
		}
		return TRUE;
	}

/// <summary>
/// Public �����
/// ����� ����� � ���������� ID 
/// </summary>
BOOL CPhoneTypesTable::SelectWhereID(const long lID, PHONE_TYPES& oPhoneType)
	{
		CDataSource oDataSource;
		CSession oSession;

		try
		{
			HRESULT hResult = CoInitialize(0);

			if (FAILED(hResult))
			{
				//Message( _T("Unable to CoInitialize COM Interface.") );
				return FALSE;
			}

			if(!ConnectToDatabase(oDataSource, oSession))
			{
				//Error connecting to the database
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			//������������ ��������
			CString strQuery;
			strQuery.Format(_T("SELECT * FROM PHONE_TYPES WHERE ID=%d"),lID);

			// ����������� ���������
			hResult = Open(oSession, strQuery);

			if (FAILED(hResult))
			{
				//Message(_T("Error executing query. Error: %d. Query: %s"), hResult, strQuery);
				oSession.Close();
				oDataSource.Close();
				Close();
				return FALSE;
			}

			hResult = MoveNext();

			if (FAILED(hResult))
			{
				//Error empty table
				oSession.Close();
				oDataSource.Close();
				Close();
				return FALSE;
			}
			
			oPhoneType.lID = m_PhoneType.lID;
			oPhoneType.lUpdateCounter = m_PhoneType.lUpdateCounter;
			memcpy(oPhoneType.szPhoneType, m_PhoneType.szPhoneType, sizeof(m_PhoneType.szPhoneType));

			// ��������� ���������, ������� � �������� � ������ �����. 
			Close();
			oSession.Close();
			oDataSource.Close();
		}
		catch(HRESULT)
		{
			return FALSE;
		}
		return TRUE;
	} 

/// <summary>
/// Public �����
/// ������ �����
/// </summary>
BOOL CPhoneTypesTable::Insert(const PHONE_TYPES& oPhoneType)
	{
		CDataSource oDataSource;
		CSession oSession;

		try
		{
			HRESULT hResult = CoInitialize(0);

			if (FAILED(hResult))
			{
				//Message( _T("Unable to CoInitialize COM Interface.") );
				return FALSE;
			}

			if(!ConnectToDatabase(oDataSource, oSession))
			{
				//Error connecting to the database
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}

			//������������ ��������
			CString strQuery;
			strQuery = _T("SELECT * FROM PHONE_TYPES");

			if(!CreateModifyingSession(strQuery,oSession))
			{
				//Error opening session
				oDataSource.Close();
				oSession.Close();
				Close();
				return FALSE;
			}
			
			memcpy(m_PhoneType.szPhoneType, oPhoneType.szPhoneType, sizeof(oPhoneType.szPhoneType));
			m_PhoneType.lUpdateCounter = oPhoneType.lUpdateCounter;

			hResult = CRowset::Insert(1,true);

			if (FAILED(hResult))
			{
				//Error inserting data
				oSession.Close();
				oDataSource.Close();
				return FALSE;
			} 

			hResult = Update();

			if (FAILED(hResult))
			{
				//Error updating table
				oSession.Close();
				oDataSource.Close();
				return FALSE;
			} 

			// ��������� ���������, ������� � �������� � ������ �����. 
			Close();
			oSession.Close();
			oDataSource.Close();
		}
		catch(HRESULT)
		{
			return FALSE;
		}

		return TRUE;
	}

/// <summary>
/// Public �����
/// ������� �� ����� � ���������� ID 
/// </summary>
BOOL CPhoneTypesTable::UpdateWhereID(const long lID, const PHONE_TYPES& oPhoneType)
{
	CDataSource oDataSource;
	CSession oSession;

	try
	{
		HRESULT hResult = CoInitialize(0);

		if (FAILED(hResult))
		{
			//Message( _T("Unable to CoInitialize COM Interface.") );
			return FALSE;
		}

		if(!ConnectToDatabase(oDataSource, oSession))
		{
			//Error connecting to the database
			oDataSource.Close();
			oSession.Close();
			Close();
			return FALSE;
		}

		//������������ ��������
		CString strQuery;
		strQuery.Format(_T("SELECT * FROM PHONE_TYPES WHERE ID=%d"),lID);

		if(!CreateModifyingSession(strQuery ,oSession))
		{
			//Error opening session
			oDataSource.Close();
			oSession.Close();
			Close();
			return FALSE;
		}

		hResult = MoveNext();

		if(FAILED(hResult))
		{
			//Error empty table
			oSession.Close();
			oDataSource.Close();
			Close();
			return FALSE;
		}
		
		memcpy(m_PhoneType.szPhoneType, oPhoneType.szPhoneType, sizeof(oPhoneType.szPhoneType));
		m_PhoneType.lUpdateCounter+=1;

		hResult = SetData(1);

		if(FAILED(hResult))
		{
			//Error setting data
			oSession.Close();
			oDataSource.Close();
			Close();
			return FALSE;
		}

		hResult = Update();

		if(FAILED(hResult))
		{
			//Error on update
			oSession.Close();
			oDataSource.Close();
			Close();
			return FALSE;
		}
		// ��������� ���������, ������� � �������� � ������ �����. 
		Close();
		oSession.Close();
		oDataSource.Close();
	}
	catch(HRESULT)
	{
		return FALSE;
	}
	return TRUE;
};

/// <summary>
/// Private �����
/// ����������� ������ � ���� �������
/// </summary>
BOOL CPhoneTypesTable::ConnectToDatabase(CDataSource& oDataSource, CSession& oSession)
{
	CDBPropSet oDBPropSet(DBPROPSET_DBINIT);
	oDBPropSet.AddProperty(DBPROP_INIT_DATASOURCE,	_T("LENOVO2\\SQL2008") );	// ������
	oDBPropSet.AddProperty(DBPROP_AUTH_USERID,		_T("sa") );			// ����������
	oDBPropSet.AddProperty(DBPROP_AUTH_PASSWORD,	_T("massive") );			// ������
	oDBPropSet.AddProperty(DBPROP_INIT_CATALOG,		_T("PhoneBookDB" ));	// ���� �����
	oDBPropSet.AddProperty(DBPROP_AUTH_PERSIST_SENSITIVE_AUTHINFO,	false);
	oDBPropSet.AddProperty(DBPROP_INIT_LCID,		1033L);
	oDBPropSet.AddProperty(DBPROP_INIT_PROMPT,		static_cast<short>(4));

	// ��������� �� ��� ������ �����
	HRESULT hResult = oDataSource.Open(_T("SQLOLEDB.1"), &oDBPropSet);

	if (FAILED(hResult))
	{
		//Message(_T("Unable to connect to SQL Server database. Error: %d"), hResult);
		return FALSE;
	}

	// �������� �����
	hResult = oSession.Open(oDataSource);

	if (FAILED(hResult))
	{
		//Message(_T("Unable to open session. Error: %d"), hResult);
		return FALSE;
	}

	return TRUE;
}

/// <summary>
/// Private �����
/// ������� ����� � ����� �� �������/��������� �� �����
/// </summary>
BOOL CPhoneTypesTable::CreateModifyingSession(CString strQuery, CSession& oSession)
{
	CDBPropSet oUpdateDBPropSet(DBPROPSET_ROWSET);
	oUpdateDBPropSet.AddProperty(DBPROP_CANFETCHBACKWARDS, true);
	oUpdateDBPropSet.AddProperty(DBPROP_IRowsetScroll, true);
	oUpdateDBPropSet.AddProperty(DBPROP_IRowsetChange, true);
	oUpdateDBPropSet.AddProperty(DBPROP_UPDATABILITY, DBPROPVAL_UP_CHANGE | DBPROPVAL_UP_INSERT | DBPROPVAL_UP_DELETE);
	oUpdateDBPropSet.AddProperty(DBPROP_IRowsetUpdate, true); 

	// ����������� ���������
	HRESULT hResult = Open(oSession, strQuery, &oUpdateDBPropSet);
	if (FAILED(hResult))
	{
		//Message(_T("Error executing query. Error: %d. Query: %s"), hResult, strQuery);
		return FALSE;
	}

	return TRUE;
}