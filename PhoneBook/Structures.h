#include "stdafx.h"

#define NAME_SIZE 32
#define MIDDLE_NAME_SIZE 32
#define LAST_NAME_SIZE 32
#define AREA_SIZE 32
#define NUMBER_SIZE 16
#define ADDRESS_SIZE 255
#define UCN_SIZE 10

/// <summary>
/// ��������� �������� ��������� PERSONS
/// </summary>
struct PERSONS
{
//�������� �������������
	int  nID;
	//����� �� ���������
	int  nUpdateCounter;
	//����� ���
	char szFirstName[NAME_SIZE+1];
	//������ ���
	char szMiddleName[MIDDLE_NAME_SIZE+1];
	//�������
	char szLastName[LAST_NAME_SIZE+1];
	//���
	char szUCN[UCN_SIZE+1];
	//������������� �� �����
	int  nCityID;
	//�����
	char szAddress[ADDRESS_SIZE+1];

	//�������� �� �������
	PERSONS()
	{
		SecureZeroMemory(this, sizeof(PERSONS));
	}
	
	/// <summary>
	///����������� ��������� � ���������
	/// </summary>
	PERSONS(int nID, int nUpdateCounter, char szFirstName[NAME_SIZE], char szMiddleName[NAME_SIZE], char szLastName[NAME_SIZE], char szUCN[UCN_SIZE], int nCityID, char szAddress[ADDRESS_SIZE])
	{
		SecureZeroMemory(this, sizeof(PERSONS));
		this->nID = nID;
		this->nUpdateCounter = nUpdateCounter;
		this->nCityID = nCityID;
		memcpy(this->szFirstName, szFirstName, sizeof(this->szFirstName));
		memcpy(this->szMiddleName, szMiddleName, sizeof(this->szMiddleName));
		memcpy(this->szLastName, szLastName, sizeof(this->szLastName));
		memcpy(this->szUCN, szUCN, sizeof(this->szUCN));
	}
};

//��������� ���. ����� �� ��������� PERSONS*
typedef CTypedPtrArray<CPtrArray, PERSONS*> CPersonsArray;

/// <summary>
/// ��������� �������� ��������� CITIES
/// </summary>
struct CITIES
{
	//�������� �������������
	long  lID;
	//����� �� ���������
	long  lUpdateCounter;
	//��� �� �����
	TCHAR szName[NAME_SIZE+1];
	//����� �� �����
	TCHAR szArea[NAME_SIZE+1];

	//�������� �� �������
	CITIES()
	{
		SecureZeroMemory(this, sizeof(CITIES));
	}

	CITIES(long lID, long lUpdateCounter, TCHAR szName[NAME_SIZE+1], TCHAR szArea[NAME_SIZE+1])
	{
		SecureZeroMemory(this, sizeof(CITIES));
		this->lID = lID;
		this->lUpdateCounter = lUpdateCounter;
		memcpy(this->szName, szName, sizeof(this->szName));
		memcpy(this->szArea, szArea, sizeof(this->szArea));
	}
};

//��������� ���. ����� �� ��������� CITIES*
typedef CTypedPtrArray<CPtrArray, CITIES*> CCitiesArray;


/// <summary>
/// ��������� �������� ��������� PHONE_TYPES
/// </summary>
struct PHONE_TYPES
{
	//�������� �������������
	int  nID;
	//����� �� ���������
	int  nUpdateCounter;
	//��� �� ���������� �����
	char szPhoneType[NAME_SIZE+1];

	//�������� �� �������
	PHONE_TYPES()
	{
		SecureZeroMemory(this, sizeof(PHONE_TYPES));
	}
};

//��������� ���. ����� �� ��������� PHONE_TYPES*
typedef CTypedPtrArray<CPtrArray, PHONE_TYPES*> CPhoneTypesArray;


/// <summary>
/// ��������� �������� ��������� PHONE_NUMBERS
/// </summary>
struct PHONE_NUMBERS
{
	//�������� �������������
	int  nID;
	//����� �� ���������
	int  nUpdateCounter;
	//������������� �� �����������
	int  nPersonID;
	//������������� �� ���� �� ���������� �����
	int  nPhoneTypeID;
	//��������� �����
	char szPhoneNumber[NUMBER_SIZE+1];

	//�������� �� �������
	PHONE_NUMBERS()
	{
		SecureZeroMemory(this, sizeof(PHONE_NUMBERS));
	}
};

//��������� ���. ����� �� ��������� PHONE_NUMBERS*
typedef CTypedPtrArray<CPtrArray, PHONE_NUMBERS*> CPhoneNumbersArray;