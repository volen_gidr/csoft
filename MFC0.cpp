// MFC0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MFC0.h"
#include "Structures.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			nRetCode = 1;
		}
		else
		{
			//CString example
			CString oString1(_T("This is a test"));
			CString oString2(_T(" CString"));

			cout<< "CString example: " << endl;
			wcout << oString1.GetString() << endl;
			oString1 += oString2;
			wcout << oString1.GetString() << endl;
			oString1.Replace(_T("This"), _T("That"));
			wcout << oString1.GetString() << endl;
			cout << endl;

			//CArray example
			CArray<int> oArray1, oArray2;

			oArray1.Add(1);
			oArray1.Add(2);
			oArray1.Add(3);
			oArray1.Add(4);
			oArray1.Add(5);

			oArray2.Add(5);
			oArray2.Add(4);
			oArray2.Add(3);
			oArray2.Add(2);
			oArray2.Add(1);
			
			cout<< "CArray example: " << endl;

			cout<< "oArray1: ";
			for	( int i = 0 ; i < oArray1.GetSize() ; i++ ) 
			{
				cout << oArray1.ElementAt(i) << " ";
			}
			cout << endl;

			cout<< "oArray2: ";
			for	( int i = 0 ; i < oArray2.GetSize() ; i++ ) 
			{
				cout << oArray2.ElementAt(i) << " ";
			}
			cout << endl;

			cout<< "oArray1 + oArray2 using Append: ";
			oArray1.Append(oArray2);
			for	( int i = 0 ; i < oArray1.GetSize() ; i++ ) 
			{
				cout << oArray1.ElementAt(i) << " ";
			}
			cout << endl;
			
			oArray1.InsertAt(5,0);
			cout<< "Adding new element (0) at user defined index: ";
			for	( int i = 0 ; i < oArray1.GetSize() ; i++ ) 
			{
				cout << oArray1.ElementAt(i) << " ";
			}
			cout << endl << endl;

			//CMap example
			CMap<CString*, CString*, int, int> oMap;
			CString oMapString1("one");
			CString oMapString2("two");
			CString oMapString3("three");
			oMap[&oMapString1] = 1;
			oMap[&oMapString2] = 2;
			oMap[&oMapString3] = 3;
			cout<< "CMap example: " << endl;
			wcout<<"Key: " << oMapString1.GetString()  << " Value: " << oMap[&oMapString1] << endl;
			wcout<<"Key: " << oMapString2.GetString()  << " Value: " << oMap[&oMapString2] << endl;
			wcout<<"Key: " << oMapString3.GetString()  << " Value: " << oMap[&oMapString3] << endl;

			int nNumber;
			oMap.Lookup(&oMapString2 , nNumber);
			wcout<< "Value associated to key '" << oMapString2.GetString() << "' is: " << nNumber << endl;
			

			//CTypedPtrArray example
			PERSONS* pPERSONS1 = new PERSONS(1, 0, "Volen", "Todorov", "Todorov", "1000000000", 1, "Some address 1");
			PERSONS* pPERSONS2 = new PERSONS(2, 0, "Ivan", "Ivanov", "Ivanov", "2000000000", 1, "Some address 2");
			PERSONS* pPERSONS3 = new PERSONS(3, 0, "Stefan", "Todorov", "Todorov", "3000000000", 1, "Some address 3");

			CPersonsArray oCPersonArray1;

			oCPersonArray1.Add(pPERSONS1);
			oCPersonArray1.Add(pPERSONS2);
			oCPersonArray1.Add(pPERSONS3);
			int* pTestPointer = NULL;
			//oCPersonArray1.Add(pTestPointer); This is an error since the pointer type needs to be 'PERSONS' not 'int'
			cout << "\nCTypedPtrArray example:\nID	 Full Name			UCN		CityID		Address\n";

			PERSONS* pPERSONSTemp;
			for( int i = 0 ; i < oCPersonArray1.GetSize() ; i++ )
			{
				pPERSONSTemp = oCPersonArray1.ElementAt(i);
				cout	<< pPERSONSTemp->nID << "	"<< pPERSONSTemp->szFirstName<< " " << pPERSONSTemp->szMiddleName<< " " << pPERSONSTemp->szLastName << "		"
						<< pPERSONSTemp->szUCN<< "	" << pPERSONSTemp->nCityID<< "	" << pPERSONSTemp->szAddress << endl;
			}

			//CPtrArray example
			void* pVoid1 = new void*;
			void* pVoid2 = new void*;
			void* pVoid3 = new void*;
			double* pDouble1 = new double;

			CPtrArray oCPtrArray1;
			cout<<"\nCPtrArray example: \n";
			oCPtrArray1.Add(pDouble1); //Not an error, but will lead to unexpected behaviour
			oCPtrArray1.Add(pVoid1);
			oCPtrArray1.Add(pVoid2);
			oCPtrArray1.Add(pVoid3);

			for ( int i = 0 ; i < oCPtrArray1.GetSize() ; i++ )
			{
				cout << oCPtrArray1.ElementAt(i) << endl;
			}
			
			getchar();
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		nRetCode = 1;
	}

	return nRetCode;
}
